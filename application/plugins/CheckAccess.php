<?php
class CheckAccess extends Zend_Controller_Plugin_Abstract {

    public function preDispatch(Zend_Controller_Request_Abstract $request) {
        $acl = Zend_Controller_Front::getInstance()->getParam('bootstrap')->getResource('Acl');

        if (!$acl->can()){
            $request = $this->getRequest();

            /*
            $request->setControllerName ('error');
            $request->setActionName('denied');
             */

            $request->setControllerName ('auth');
            $request->setActionName('login');

            $meetId = $request->getParam('meetId') ? $request->getParam('meetId') : $request->getParam('meetingInstanceId');
            $userId = $request->getParam('userId') ? $request->getParam('userId') : $request->getParam('contactId');
            $pass = $request->getParam('pass') ? $request->getParam('pass') : $request->getParam('password');
            $request->setParams(array('meetingInstanceId'=>$meetId,'contactId'=>$userId,'pass'=>$pass));



        }

    }

}