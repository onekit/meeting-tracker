<?php
require_once('common.php');

class Bootstrap extends Zend_Application_Bootstrap_Bootstrap
{


    public function _initAuth()
    {
        $auth = Zend_Auth::getInstance();
        $data = $auth->getStorage()->read();
        if (!isset($data->status)) {
            $storage_data = new stdClass();
            $storage_data->status = 'guest';
            $auth->getStorage()->write($storage_data);
        }
    }

    public function _initAcl()
    {
        Zend_Loader::loadClass('Acl');
        Zend_Loader::loadClass('CheckAccess');
        Zend_Controller_Front::getInstance()->registerPlugin(new CheckAccess());
        return new Acl();
    }

    protected function _initView()
    {
        $view = new Zend_View();
        $view->doctype('XHTML1_STRICT');
        $view->env = APPLICATION_ENV;
        $view->headMeta()->appendHttpEquiv('Content-Type', 'text/html;charset=utf-8');
        $view->addHelperPath("ZendX/JQuery/View/Helper", "ZendX_JQuery_View_Helper");
        $view->jQuery()->setLocalPath('/js/jquery-1.7.1.min.js')
                ->setUiLocalPath('/js/jquery-ui-1.8.13.custom.min.js')
                ->addStyleSheet('/css/jquery-ui-1.8.13.custom.css');
        $view->jQuery()->enable();
        $view->jQuery()->uiEnable();
        $viewRenderer = new Zend_Controller_Action_Helper_ViewRenderer();
        $viewRenderer->setView($view);
        Zend_Controller_Action_HelperBroker::addHelper($viewRenderer);

        return $view;

    }


    public function _initRoutes()
    {

        $frontController = Zend_Controller_Front::getInstance();
        $router = $frontController->getRouter();


        $route = new Zend_Controller_Router_Route (
            'auth/login/:meetingInstanceId/:contactId/:pass',
            array('controller' => 'auth',
                 'action' => 'login',
                 'meetingInstanceId' => '',
                 'contactId' => '',
                 'pass' => '',
            )
        );
        $router->addRoute('auth', $route);


        $route = new Zend_Controller_Router_Route (
            'content/:meetId/:userId/:pass',
            array('controller' => 'content',
                 'action' => 'index',
                 'meetId' => '',
                 'userId' => '',
                 'pass' => '',
            )
        );
        $router->addRoute('content', $route);


        $route = new Zend_Controller_Router_Route (
            'comment/list/:meetId/:contentId/:userId',
            array('controller' => 'comment',
                 'action' => 'list',
                 'meetId' => 'meetId',
                 'contentId' => 'contentId',
                 'userId' => 'userId',
            )
        );

        $router->addRoute('commentList', $route);


        $route = new Zend_Controller_Router_Route (
            'comment/add/:meetId/:contentId/:userId',
            array('controller' => 'comment',
                 'action' => 'add',
                 'meetId' => 'meetId',
                 'contentId' => 'contentId',
                 'userId' => 'userId',
            )
        );

        $router->addRoute('commentAdd', $route);

        $route = new Zend_Controller_Router_Route (
            'comment/delete/:commentId/:userId',
            array('controller' => 'comment',
                 'action' => 'delete',
                 'commentId' => 'commentId',
                 'userId' => 'userId',
            )
        );
        $router->addRoute('commentDelete', $route);


        $route = new Zend_Controller_Router_Route (
            'comment/restore/:commentId/:userId',
            array('controller' => 'comment',
                 'action' => 'restore',
                 'commentId' => 'commentId',
                 'userId' => 'userId',
            )
        );
        $router->addRoute('commentRestore', $route);


        $route = new Zend_Controller_Router_Route (
            'comment/dashboard/:meetId/:contentId/:userId',
            array('controller' => 'comment',
                 'action' => 'index',
                 'meetId' => 'meetId',
                 'contentId' => 'contentId',
                 'userId' => 'userId',
            )
        );

        $router->addRoute('commentIndex', $route);


        $route = new Zend_Controller_Router_Route (
            'comment/count/:meetId',
            array('controller' => 'comment',
                 'action' => 'count',
                 'meetId' => 'meetId',

            )
        );

        $router->addRoute('commentCount', $route);

        $route = new Zend_Controller_Router_Route (
            'comment/get/:commentId',
            array('controller' => 'comment',
                 'action' => 'get',
                 'commentId' => 'commentId',

            )
        );

        $router->addRoute('commentGet', $route);



        $route = new Zend_Controller_Router_Route (
            'privacy-policy',
            array('controller' => 'Index',
                'action' => 'privacy'
            )
        );

        $router->addRoute('privacy', $route);





    }



    protected function _initTranslate()
    {

        

        $tr = new Zend_Translate('array', APPLICATION_PATH . '/languages/en.php', 'en');
        //$tr = new Zend_Translate('gettext', APPLICATION_PATH . '/languages/ru.mo', 'ru');
        $tr->addTranslation(APPLICATION_PATH . '/languages/ru.php', 'ru');


        $session = new Zend_Session_Namespace('session');
        $langLocale = isset($session->lang) ? $session->lang : new Zend_Locale('en_US');
        Zend_Registry::set('Zend_Locale', new Zend_Locale($langLocale));
        Zend_Registry::set('Zend_Translate', $tr);
        Zend_Registry::set('Zend_Session_Namespace', $session);






    }


}
