<?php

class AuthController extends Zend_Controller_Action
{

    public function init()
    {

    }

    public function indexAction()
    {
        $this->_forward("login");
    }


    public function loginAction()
    {

        $params = $this->getRequest()->getParams();
        $form = new Application_Form_Login();

        //$params['meetingInstanceId'] = @$params['meetingInstanceId'] ? $params['meetingInstanceId'] : $params['meetId'];
        //$params['contactId'] = @$params['contactId'] ? $params['contactId'] : $params['userId'];

        if (@$params['meetingInstanceId']) {
            $form->addElement('hidden', 'meetingInstanceId', array('value' => $params['meetingInstanceId']));
        }
        if (@$params['contactId']) {
            $form->addElement('hidden', 'contactId', array('value' => $params['contactId']));
        }
        /*
        if (@$params['pass']) {
            $form->addElement('password','pass',array('value'=>$params['pass']));
        }
        */


        if ($form->isValid($this->getRequest()->getPost()) OR $form->isValid($params)) {

            $meetingInstanceId = $form->meetingInstanceId->getValue() ? $form->meetingInstanceId->getValue() : $params["meetingInstanceId"];
            $contactId = $form->contactId->getValue() ? $form->contactId->getValue() : $params["contactId"];
            $pass = $form->pass->getValue() ? $form->pass->getValue() : $params["pass"];


            $bootstrap = $this->getInvokeArg('bootstrap');
            $auth = Zend_Auth::getInstance();
            $adapter = $bootstrap->getPluginResource('db')->getDbAdapter();
            $authAdapter = new Zend_Auth_Adapter_DbTable($adapter);


            $authAdapter->setTableName('meetingpassword');
            $authAdapter->setIdentityColumn('meetingInstanceId_contactId')
                        ->setCredentialColumn('password');

            $authAdapter->setIdentity($meetingInstanceId.$contactId)
                ->setCredential($pass);
            //don't hash like before //->setCredential(hashPass($pass));


            $result = $auth->authenticate($authAdapter);

            //if successful, then save info about user
            if ($result->isValid()) {

                $storage = $auth->getStorage();
                $storage_data = $authAdapter->getResultRowObject(
                    null,
                    array('activate', 'pass', 'enabled'));

                $storage_data->status = 'user';
                $storage->write($storage_data);
                //$this->_redirect('/content/'.$meetingInstanceId.'/'.$contactId);
                $this->_forward('index', 'content', null, array('meetId' => $meetingInstanceId, 'userId' => $contactId));

            } else {
                $form->addError('Wrong credentials');
            }


        }
        $this->view->errorMessages = $form->getErrorMessages();
        $this->view->form = $form;


    }


    public function logoutAction()
    {
        Zend_Auth::getInstance()->clearIdentity();
        $this->_helper->redirector('index'); // back to login page
    }

}
