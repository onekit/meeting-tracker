<?php

class XmlController extends Zend_Controller_Action
{

    public function init()
    {
        /* Initialize action controller here */

    }

    public function indexAction()
    {
        //action body


    }

    public function saveAction()
    {
        // action body
        $this->view->headTitle("Meeting Tracker");

        //getting XML from RawPostData or file of form


        if ($this->getRequest()->isPost() OR $this->getRequest()->getRawBody()) {

            if (isset($_FILES["file"]["tmp_name"])) {
                $xmlstr = file_get_contents($_FILES["file"]["tmp_name"]);
            } else {
                $xmlstr = $this->getRequest()->getRawBody();
            }

            $this->_helper->layout()->setLayout('empty');

            $xml = simplexml_load_string($xmlstr);
            $xmlMeet = $xml->attributes();

            $xmlTopics = $xml->topicInstance;
            $xmlContacts = $xml->contacts;

            //validate Art-Mobile Meeting format
            if (!$xmlMeet OR !$xmlTopics) {
                throw new Exception("XML not consist tags 'topicInstance'  and 'meetingInstance' have no attributes");
            }

            //log XML files which came from Android Application
            file_put_contents('xmls/' . $xmlMeet . '.xml', $xmlstr);

            $dbMeet = new Application_Model_DbTable_MeetingInstance();
            $dbPassword = new Application_Model_DbTable_MeetingPassword();
            $dbContacts = new Application_Model_DbTable_Contact();
            $dbTopics = new Application_Model_DbTable_TopicInstance();
            $dbRecord = new Application_Model_DbTable_Record();

            //check if exist
            $check_meet = $dbMeet->getMeetingInstance(cid($xmlMeet['id']));
            $is_update = false;
            if (isset($check_meet['id'])) {
                $is_update = true; //not new
                //get users passwords
                $passwords_array = $dbPassword->getPasswordsByMeetingInstance($check_meet['id']);
                foreach ($passwords_array as $pass) {
                    $passwords[$pass['contactId']] = $pass['password']; //save passwords array to assign them to new meeting
                }
                $dbMeet->delMeetingInstance(cid($check_meet['id']));
            }


            //begin of adding new metting
            $xmlOwnerId = $xmlMeet['ownerId']; //ignore incoming ownderId but save to recognize owner
            //$ownerId = genId(); //new ownerId
            $ownerId = $xmlOwnerId; //assign ownerId
            $meetId = cid($xmlMeet['id']);
            $meetTitle = $xmlMeet['title'];


            //let's add new meeting
            $dbMeet->addMeetingInstance($meetId, $meetTitle, $ownerId, $xmlMeet['startTime'], $xmlMeet['expirationDate']);
            //$ownerId = trim($xmlMeet['ownerId']);

            //add meeting topics
            foreach ($xmlTopics as $xmlTopic) {

                $topic = $xmlTopic->attributes();
                $records = $xmlTopic->records;
                $dbTopics->addTopic(cid($topic['id']), $topic['title'], $topic['order'], $meetId);

                //add records for this topic
                foreach ($records->record as $record) {
                    $cleanRecord = cleanRecord($record);
                    if ($record == '' && $record['type'] != 'text') {
                        $record['type'] = 'patch';
                    } //if empty tag, that wrong input data
                    $dbRecord->addRecord($record['id'], $record['type'], $record['order'], $record['provider'], $cleanRecord, $topic['id'], $meetId);
                }
            }

            //add contacts
            $ownerPassword = null;
            if ($xmlContacts->contact) {
                foreach ($xmlContacts->contact as $xmlContact) {
                    $contact = $xmlContact->attributes();

                    //$contactId = genId();
                    $contactId = cid($contact['id']);
                    $password = isset($passwords[$contactId]) ? $passwords[$contactId] : genPass();


                    if ($contact['isRecipient'] == true) {
                        $contactArr = get_object_vars($contact);
                        $contactItem = $contactArr['@attributes'];

                        if ($xmlOwnerId == $contactItem['id']) {
                            $contactItem['owner'] = true;
                            $ownerPassword = $password;
                            $contactId = $ownerId;
                        }
                        $contactItem['id'] = $contactId;
                        $contactItem['password'] = $password;

                        $recipients[] = $contactItem;
                    }

                    $dbContacts->addContact($contactId, $contact['name'], $contact['email'], $contact['isParticipant'], $contact['isRecipient'], $meetId);
                    //add to ACL the rights for users
                    /*
                    if (!$is_update) { //hashing password if it is a new
                        $password = hashPass($password);
                    }
                    not hash password, because we need to keep them as is (for email notifications).
                    */

                    $dbPassword->addPassword(genId(), $meetId, $contactId, $password);


                }
            }
            //end of adding new meeting


            //begin of emailing
            if (isset($recipients)) {

                $attachments = array();
                $sourceUrl = 'http://' . $_SERVER['HTTP_HOST'] . '/content/' . $meetId . '/' . $ownerId . '/' . $ownerPassword;
                //$docX = generate_docx($sourceUrl);
                $pdf = generate_pdf($sourceUrl);
                //$attachments[] = $docX;
                $attachments[] = $pdf;

                foreach ($recipients as $recipient) {
                    $url = 'http://' . $_SERVER['HTTP_HOST'] . '/content/' . $meetId . '/' . $recipient['id'];
                    mail_meeting($meetId, $meetTitle, $recipient['email'], $recipient['name'], $url, $recipient['password'], $attachments, $is_update);
                }


            }
            //end of emailing

            //return the owner url with auto-login to client side
            $url = 'http://' . $_SERVER['HTTP_HOST'] . '/content/' . $meetId . '/' . $ownerId . '/' . $ownerPassword;
            $this->view->url = $url;


        } else {
            $form = new Application_Form_PostXML();
            $form->submit->setLabel('Send');
            $this->view->form = $form;
            echo $this->view->form;
        }
    }


    public function sendAction()
    {

        $this->_helper->layout()->setLayout('empty');
        $xml = file_get_contents("example.xml");
        $client = new Zend_Http_Client('http://' . $_SERVER['HTTP_HOST'] . '/xml/save', array('keepalive' => true));
        $client->setRawData($xml, 'text/xml');
        $response = $client->request(Zend_Http_Client::POST);

        echo isset($response) ? "Ok" : "Error";
        return $response;


    }


}


function cleanRecord($record)
{

    if ($record['type'] == 'text') {
        return nl2br($record);
    }

    if ($record['type'] == 'video' & stristr($record, 'youtube') == true) { //youtube url (not ID)
        $url_string = parse_url($record, PHP_URL_QUERY);
        parse_str($url_string, $args);
        $result = isset($args['v']) ? $args['v'] : false;
        return $result;
    }

    if ($record['type'] == 'audio' AND stristr($record, "soundcloud.com")) { //$record['provider'] == 'soundcloud'
        $result = soundcloudGetTrackIdByURL($record);
        return $result;
    }



    return $record;

}

function soundcloudGetTrackIdByURL($url)
{

            $client = new Zend_Http_Client();
            $client->setUri('http://soundcloud.com/oembed?format=json&url=' . $url);
            $response = $client->request('GET');
            $body = $response->getBody();

        //begin of patch for long delay of SoundCloud
        if (strlen($body)<10) {
            sleep(5);
            $response = $client->request('GET');
            $body = $response->getBody();
        }
        //end of patch



        if (strlen($body)>1) {
            $data = Zend_Json::decode($body);
            //parse track id from html code provided by SoundCloud
            $result = explode('2F',$data['html']);
            $result = explode('"',$result[4]);
            return $result[0];
        }

        return null;
}


function mail_meeting($meetId, $meetTitle, $email, $username, $url, $password, $attachments = null, $update = null)
{

    //detect language
    require_once('lang.php');
    $lang = language_detect($meetTitle);
    $tr = Zend_Registry::get('Zend_Translate');
    $tr->setLocale($lang);
    //change locale to most prevalent language

    if ($update) {

        $subject = $tr->translate('Meeting') . ' "' . $meetTitle . '" ' . $tr->translate('has been updated');
        $message = $tr->translate('Dear') . ' ' . $username . '!
' . $tr->translate('This message posted by Meeting Tracker.') . '
' . $tr->translate('Meeting') . ' "' . $meetTitle . '" ' . $tr->translate('is available on the net:') . '
' . $url . '/' . $password . '
' . $tr->translate('Best regards,') . '
' . $tr->translate('Meeting Tracker Service');

    } else {
        $subject = $tr->translate('Meeting') . ' "' . $meetTitle . '"';
        $message = $tr->translate('Dear') . ' ' . $username . '!
' . $tr->translate('This message posted by Meeting Tracker.') . '
' . $tr->translate('Meeting') . ' "' . $meetTitle . '" ' . $tr->translate('is available on the net:') . '
' . $url . ' ' . $tr->translate('by password:') . ' ' . $password . '

' . $tr->translate('Best regards,') . '
' . $tr->translate('Meeting Tracker Service') . '


Meeting ID: ' . $meetId . '
' . $tr->translate('P.S.: You can append password to link for auto-login like this:') . '

' . $url . '/' . $password . ' ';
    }

    $mail = new Zend_Mail('UTF-8');


    foreach ($attachments as $attach) {
        $at = new Zend_Mime_Part($attach['content']);
        $at->type = $attach['type'];
        $at->disposition = Zend_Mime::DISPOSITION_INLINE;
        $at->encoding = Zend_Mime::ENCODING_BASE64;
        $at->filename = $attach['filename'];
        $mail->addAttachment($at);
    }

    $mail->setFrom('noreply@' . $_SERVER['HTTP_HOST'], 'Meeting Tracker');
    $mail->addTo($email);
    $mail->setSubject($subject);
    $mail->setBodyText($message);
    $mail->send();

}


// Documents generating starts here

function generate_pdf($url)
{

    $client = new Zend_Http_Client();
    $url = trim($url) . '?format=puretext';
    $client->setUri($url);
    $response = $client->request();
    $html = $response->getBody();


    /*
    * Solution with library MPDF53 - the best solution with full colored and styled PDF file
    */

    require_once('MPDF53/mpdf.php');
    $mpdf = new mPDF('utf-8', 'A4');
    $stylesheet = file_get_contents('css/style.css');
    $mpdf->WriteHTML($stylesheet, 1);
    $mpdf->WriteHTML($html);
    $result = $mpdf->Output('', 'S');

    $file['filename'] = 'report.pdf';
    $file['type'] = 'application/pdf';
    $file['content'] = $result;

    return $file;

}

function generate_docx($url)
{

    $url = trim($url) . '?format=puretext';

    require_once("html2docx/html2docx.php");
    $html = new html2docx();
    $result = $html->convert($url);

    $file['filename'] = 'report.docx';
    $file['type'] = 'application/vnd.ms-word.document';
    $file['content'] = $result;

    return $file;

}