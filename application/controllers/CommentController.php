<?php

class CommentController extends Zend_Controller_Action
{

    public function init()
    {
        $this->_helper->layout()->setLayout('empty');

        $session = Zend_Registry::get('Zend_Session_Namespace');
        $langDefault = $session->lang;
        //get language from session

        $tr = Zend_Registry::get('Zend_Translate');
        $tr->setLocale($langDefault);

        $this->view->comments_title = $this->view->translate("Comments");
        $this->view->no_comments = $this->view->translate("No comments");


    }

    public function indexAction()
    {

        $meetId = cid($this->getRequest()->getParam('meetId'));
        $contentId = cid($this->getRequest()->getParam('contentId'));
        $userId = cid($this->getRequest()->getParam('userId'));
        $this->view->MeetingId = $meetId;
        $this->view->ContentId = $contentId;
        $this->view->UserId = $userId;
        //content loading in template via jQuery



    }

    public function addAction()
    {

        $meetId = cid($this->getRequest()->getParam('meetId'));
        $contentId = cid($this->getRequest()->getParam('contentId'));
        $userId = cid($this->getRequest()->getParam('userId'));
        $this->view->MeetingId = $meetId;
        $this->view->ContentId = $contentId;
        $this->view->UserId = $userId;

        $dbUser = new Application_Model_DbTable_Contact();
        $currentUser = $dbUser->getContact($userId);
        $this->view->user = $currentUser;

        if ($this->getRequest()->isPost()) {


            $id = genId();
            $this->view->submitted = true; //flag for javascript which will reload COMMENT-LIST div


            $dbComment = new Application_Model_DbTable_Comment();
            $content = isset($_POST['comment']) ? $_POST['comment'] : "";
            $content = str_replace("\n", "<br />", $content);

            $submitTime = time();
            $comment = $dbComment->addComment($id, $contentId, $submitTime, $content, $userId);
            $this->view->commentResult = $comment;
            $this->view->commentId = $id;
            $this->view->commentContent = $content;


        } else {

            if (isset($userId) & $userId != 'userId') {

                $form = new Application_Form_CommentForm();
                $form->setAttrib('onSubmit', 'if(!validate()){return false;}$.post("/comment/add/' . $meetId . '/' . $contentId . '/' . $userId . '", $("#CommentForm").serialize(),function(data) {$("#DivCommentAddDummy").html(data);}); return false;');
                $form->addElement('submit', 'submit');
                $form->submit->setLabel($this->view->translate('Send'));
                $this->view->form = $form;

            }


        }


    }

    public function deleteAction()
    {

        $userId = $this->getRequest()->getParam('userId');
        $commentId = $this->getRequest()->getParam('commentId');

        $dbComments = new Application_Model_DbTable_Comment();
        $currentComment = $dbComments->getComment($commentId);

        //if ($ownerId==$currentComment['ownerId']) and so on... TODO: protection for delete

        if (isset($currentComment)) { //if such comment exist, then delete
            //$dbComments->delComment($currentComment['id']); //delete forever
            $dbComments->delCommentSafe($currentComment['id']);
            $this->view->id = $currentComment['id'];
        }

    }

    public function editAction()
    {
        // action body
    }

    public function listAction()
    {

        $meetId = cid($this->getRequest()->getParam('meetId'));
        $contentId = cid($this->getRequest()->getParam('contentId'));
        $userId = cid($this->getRequest()->getParam('userId'));
        $this->view->MeetingId = $meetId;
        $this->view->ContentId = $contentId;
        $this->view->UserId = $userId;


        if (isset($userId) & $userId != 'userId') {
            $dbUser = new Application_Model_DbTable_Contact();
            $currentUser = $dbUser->getContact($userId);
        }

        if (isset($contentId) & $contentId != 'contentId') {

            $dbMeeting = new Application_Model_DbTable_MeetingInstance();
            $currentMeeting = $dbMeeting->getMeetingInstance($meetId);
            $ownerId = $this->view->ownerId = $currentMeeting['ownerId'];


            if (isset($userId) & $userId == $ownerId) {
                //another template with control buttons (to delete comment)
                $this->_helper->viewRenderer->setRender('listowner');
            }

            $dbComments = new Application_Model_DbTable_Comment();
            $comments = $dbComments->getCommentContentId($meetId, $contentId);

            $i = 0;
            $filteredComments = array();
            foreach ($comments as $comment) {
                $filteredComments[$i] = $comment;
                $filteredComments[$i]['date'] = defaultFormatDate($comment["submitTime"]);
                $i++;
            }

            $this->view->comments = $filteredComments;

        }


    }

    public function countAction()
    {
        $meetId = cid($this->getRequest()->getParam('meetId'));
        //$this->view->MeetingId = $meetId;
        $dbComments = new Application_Model_DbTable_Comment();
        $counters = $dbComments->count($meetId);
        $json = Zend_Json::encode($counters);
        $this->view->counters = $json;

    }


    public function getAction()
    {
        $commentId = cid($this->getRequest()->getParam('commentId'));
        $format = cid($this->getRequest()->getParam('format'));
        $dbComment = new Application_Model_DbTable_Comment();
        $comment = $dbComment->getComment($commentId);


        $dbUser = new Application_Model_DbTable_Contact();
        $user = $dbUser->getContact($comment['ownerId']); //comment owner

        if ($format == 'json') {
            $json = Zend_Json::encode($comment);
            $this->view->comment = $json;
        }

        if ($format == 'html' OR !$format) { //default
            $commentData[0] = $comment;
            $commentData[0]['date'] = defaultFormatDate($comment['submitTime']);
            $commentData[0]['name'] = $user['name'];
            $commentData[0]['email'] = $user['email'];

            $this->view->comments = $commentData;
            $this->_helper->viewRenderer->setRender('list');

            if ($user['id'] == $comment['ownerId']) {
                $this->_helper->viewRenderer->setRender('listowner');
            }

        }

    }

    public function restoreAction()
    {
        $commentId = cid($this->getRequest()->getParam('commentId'));
        $dbComment = new Application_Model_DbTable_Comment();
        $comment = $dbComment->restoreComment($commentId);
        $this->view->comment = $comment;


    }


}






