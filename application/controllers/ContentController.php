<?php


class ContentController extends Zend_Controller_Action
{


    public function init()
    {

    }


    public function indexAction()
    {


        $meetId = $this->getRequest()->getParam('meetId');
        $userId = $this->getRequest()->getParam('userId');
        $format = $this->getRequest()->getParam('format');
        $format = $format ? $format : 'default'; // format = puretext, format = null = default;
        $this->_helper->viewRenderer->setRender('index-' . $format);


        $dbMeet = new Application_Model_DbTable_MeetingInstance();
        $meet = $dbMeet->getMeetingInstance($meetId);
        if ($meet['id'] != $meetId) {
            throw new Exception("Could not find Meeting with such Id: $meetId");
        }

        $this->view->headTitle($meet['title']);
        $meet_date = defaultFormatDate($meet['startTime']);

        $this->view->MeetingId = $meet['id'];
        $this->view->MeetingTitle = $meet['title'];
        $this->view->MeetingOwnerId = $meet['ownerId'];
        $this->view->MeetingStartTime = $meet_date;
        $this->view->CurrentUserId = $userId;


        //detect language
        require_once('lang.php');
        $lang = language_detect($meet['title']);
        $tr = Zend_Registry::get('Zend_Translate');
        $tr->setLocale($lang);
        //change locale to most prevalent language

        //and save language for comments in the same language
        $session = Zend_Registry::get('Zend_Session_Namespace');
        $session->lang = $lang;


        $this->view->MeetingTitleName = $this->view->translate('Meeting Title');
        $this->view->MeetingLog = $this->view->translate('Meeting log');
        $this->view->participants = $this->view->translate('Participants');
        $this->view->meeting_begins_at = $this->view->translate('Meeting begins at');
        $this->view->comments = $this->view->translate('Comments');
        $this->view->message_cannot_be_empty = $this->view->translate('Message cannot be empty');
        $this->view->comment = $this->view->translate('Comment');
        $this->view->no_comments = $this->view->translate('No comments');
        $this->view->comments_to_this_topic = $this->view->translate('Comments to this topic');
        $this->view->has_been_deleted = $this->view->translate('has been deleted');
        $this->view->message = $this->view->translate('Message');
        $this->view->close = $this->view->translate('Close');
        $this->view->restore = $this->view->translate('Restore');

        $this->view->back_to_the_top = $this->view->translate('Back to the top');
        $this->view->loading = $this->view->translate('Loading');

        $dbTopics = new Application_Model_DbTable_TopicInstance();
        $topics = $dbTopics->getTopicMeetingId($meetId);


        foreach ($topics as $topic) {


            $dbRecords = new Application_Model_DbTable_Record();
            $records = $dbRecords->getRecordsTopic($topic['id']);


            $tpc = $this->view->partial('content/topic-' . $format . '.phtml', array('data' => $topic, 'records' => $records, 'view' => $this->view));
            $this->view->placeholder('topics')
                ->append($tpc);

        }


        $dbContacts = new Application_Model_DbTable_Contact();
        $contacts = $dbContacts->getParticipantMeetingId($meetId);

        if ($contacts) {
            foreach ($contacts as $contact) {
                $ctt = $this->view->partial('content/contact.phtml', array('contact' => $contact));
                $this->view->placeholder('contacts')
                    ->append($ctt);
            }
        }


    }


}


