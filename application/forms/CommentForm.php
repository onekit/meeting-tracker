<?php

class Application_Form_CommentForm extends ZendX_JQuery_Form
{

    public function init()
    {
        $this->setName('CommentForm');

        $textarea = $this->addElement('textarea', 'comment', array(
                                                                  'label' => $this->getView()->translate('Write your text here'),
                                                                  'required' => true,
                                                                  'Submit' => 'Sending',
                                                                  'rows' => '3',
                                                                  'cols' => '40',
                                                                  'validators' => array(
                                                                      array('validator' => 'StringLength', 'options' => array(0, 500))
                                                                  )
                                                             ));

        $submit = new Zend_Form_Element_Submit('submit');
        $submit->setLabel($this->getView()->translate('Send'));

        $this->addElements(array($textarea, $submit));

    }


}

