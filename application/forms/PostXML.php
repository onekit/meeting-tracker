<?php

class Application_Form_PostXML extends Zend_Form
{

    public function init()
    {
        
			$this->setName('PostXML');
			$id = new Zend_Form_Element_Hidden('id');
			$id->addFilter('Int');
			$file = new Zend_Form_Element_File('file');
			$file->setLabel('XML file')
			->setRequired(true);
			

			$submit = new Zend_Form_Element_Submit('submit');
			$submit->setAttrib('id', 'submitbutton');
			$this->addElements(array($id, $file, $submit));
		
		
    }


}

