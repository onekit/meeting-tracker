<?php

class Application_Form_Login extends Zend_Form
{

    public function init()
    {
        $this->setName("login");
        $this->setMethod('post');



        $this->addElement('text', 'meetingInstanceId', array(
                                                   'filters' => array('StringTrim', 'StringToLower'),
                                                   'validators' => array(
                                                       array('StringLength', false, array(0, 50)),
                                                   ),
                                                   'required' => true,
                                                   'label' => 'Meeting Id:',
                                              ));


        $this->addElement('text', 'contactId', array(
                                                   'filters' => array('StringTrim', 'StringToLower'),
                                                   'validators' => array(
                                                       array('StringLength', false, array(0, 50)),
                                                   ),
                                                   'required' => true,
                                                   'label' => 'Contact Id:',
                                              ));

        $this->addElement('password', 'pass', array(
                                                       'filters' => array('StringTrim'),
                                                       'validators' => array(
                                                           array('StringLength', false, array(0, 50)),
                                                       ),
                                                       'required' => true,
                                                       'label' => 'Password:',
                                                  ));

        $this->addElement('submit', 'enter', array(
                                                  'required' => false,
                                                  'ignore' => true,
                                                  'label' => 'Enter',
                                             ));



    }


}

