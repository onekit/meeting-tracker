<?php
class Acl extends Zend_Acl
{


    public function  __construct()
    {
        $this->addResource('index');
        $this->addResource('privacy-policy');
        $this->addResource('error');
        $this->addResource('auth');
        $this->addResource('content');
        $this->addResource('login', 'auth');
        $this->addResource('logout', 'auth');
        $this->addRole('guest');
        $this->addRole('user', 'guest');
        $this->addRole('admin', 'user');

        $this->deny(null, null, null);

        $this->allow('guest', 'index');
        $this->allow('guest', 'privacy-policy');
        $this->allow('user', 'index');
        $this->allow('user', 'privacy-policy');
        $this->allow('user', 'content', null, new CheckAccessToMeeting());
        $this->allow('guest', 'error');
        $this->allow('guest', 'auth');
        $this->allow('guest', 'auth', 'login');
        $this->allow('guest', 'auth', 'logout');
        $this->allow('user', 'auth', 'logout');
        $this->allow('user', 'auth', 'login');
        $this->allow('user', 'auth');
    }

    public function can($privilege = 'show')
    {
        $request = Zend_Controller_Front::getInstance()->getRequest();
        $resource = $request->getControllerName();
        if (!$this->has($resource)) return true;
        $storage_data = Zend_Auth::getInstance()->getStorage()->read();
        $role = array_key_exists('status', $storage_data) ? $storage_data->status : 'guest';

        return $this->isAllowed($role, $resource, $privilege);
    }
}

class CheckAccessToMeeting implements Zend_Acl_Assert_Interface
{
    public function assert(Zend_Acl $acl,
        Zend_Acl_Role_Interface $role = null,
        Zend_Acl_Resource_Interface $resource = null,
        $privilege = null)
    {
        $request = Zend_Controller_Front::getInstance()->getRequest();
        $meetId = $request->getParam('meetId') ? $request->getParam('meetId') : $request->getParam('meetingInstanceId');
        $userId = $request->getParam('userId') ? $request->getParam('userId') : $request->getParam('contactId');
        return $this->_meeting($meetId, $userId);
    }

    protected function _meeting($meetId, $userId)
    {


        $dbCredentials = new Application_Model_DbTable_MeetingPassword();
        $credentials = $dbCredentials->getPasswordByCredentials($meetId, $userId);

        $auth = Zend_Auth::getInstance();
        if ($auth->hasIdentity()) {
            $user_data = $auth->getStorage()->read();
        }


        if (@$user_data->password == @$credentials[0]['password'] & @$user_data->contactId == @$credentials[0]['contactId'] & @$user_data->meetingInstanceId == @$credentials[0]['meetingInstanceId']) {
            return true;
        } else return false;


    }
}