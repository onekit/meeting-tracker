<?php

return array(
    'Meeting Title' => 'Meeting Title',
    'Meeting log' => 'Meeting log',
    'No comments' => 'No comments',
    'Comments' => 'Comments',
    'Comments to this topic' => 'Comments to this topic',
    'Write your comment here' => 'Write your comment here (*CTRL+ENTER to submit):',
    'Send' => 'Send',
    'Comment' => 'Comment',
    'has been deleted' => 'has been deleted',
    'has been updated' => 'has been updated',
    'Message cannot be empty' => 'Message cannot be empty',
    'Message' => 'Message',
    'Close' => 'Close',
    'Restore' => 'Restore',
    'Back to the top' => 'back to the top',
    'Loading' => 'Loading',
    'Participants' => 'Participants',
    'Meeting begins at' => 'Meeting begins at',


    'Dear'=>'Dear,',
    'This message posted by Meeting Tracker.'=>'This message posted by Meeting Tracker.',
    'Meeting'=>'Meeting',
    'Topic'=>'Topic',
    'Text'=>'Record',
    'Image'=>'Image',
    'Audio'=>'Audio',
    'Video'=>'Video',
    'Missed data'=>'Missed data',
    'is available on the net:'=>'is available on the net:',
    'by password:'=>'by password:',
    'Best regards,'=>'Best regards,',
    'Meeting Tracker Service'=>'Meeting Tracker Service',
    'P.S.: You can append password to link for auto-login like this:'=>'P.S.: You can append password to link for auto-login like this:',
);


?>