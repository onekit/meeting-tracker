<?php
return array(
    'Meeting Title' => 'Наименование митинга',
    'Meeting log' => 'Протокол митинга',
    'No comments' => 'Комментариев нет',
    'Comments' => 'Комментарии',
    'Comments to this topic' => 'Комментарии к этой теме',
    'Write your text here' => 'Пишите ваш комментарий здесь (*CTRL+ENTER для отправки):',
    'Send' => 'Отправить',
    'Comment' => 'Комментарий',
    'has been deleted' => 'был удалён',
    'has been updated' => 'был обновлён',

    'Message cannot be empty' => 'Сообщение не может быть пустым',
    'Message' => 'Сообщение',
    'Close' => 'Закрыть',
    'Restore' => 'Восстановить',
    'Back to the top' => 'Наверх',
    'Loading' => 'Загружается',
    'Participants' => 'Участники',
    'Meeting begins at' => 'Митинг начался',

    'Dear'=>'Приветствуем,',
    'This message posted by Meeting Tracker.'=>'Это сообщение сформировано системой Meeting Tracker.',
    'Meeting'=>'Митинг',
    'Topic'=>'Тема',
    'Text'=>'Запись',
    'Image'=>'Изображение',
    'Audio'=>'Звукозапись',
    'Video'=>'Видео',
    'Missed data'=>'Информация пропущена',
    'is available on the net:'=>'доступен в сети:',
    'by password:'=>'по паролю:',
    'Best regards,'=>'C наилучшими пожеланиями,',
    'Meeting Tracker Service'=>'Служба Meeting Tracker',
    'P.S.: You can append password to link for auto-login like this:'=>'P.S.: Вы можете добавить через "/" к адресу пароль и получится ссылка с автоматическим входом без пароля:',
);

?>