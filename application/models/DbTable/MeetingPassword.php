<?php

class Application_Model_DbTable_MeetingPassword extends Zend_Db_Table_Abstract
{

    protected $_name = 'meetingpassword';


    public function getPassword($id)
    {

        $row = $this->fetchRow('id = "' . $id . '" ');
        if (!$row) {

            $result = null;

        } else {
            $result = $row->toArray();
        }
        return $result;
    }


    public function getPasswordByCredentials($meetId, $contactId)
    {

        $where = 'meetingInstanceId ="' . $meetId . '" AND contactId = "'.$contactId.'"';
        $rows = $this->fetchAll($where);

        if (!$rows) {
            //throw new Exception("Could not find row $contentId");
        }

        return $rows->toArray();
    }

    public function getPasswordsByMeetingInstance($meetId)
    {

        $where = 'meetingInstanceId ="' . $meetId . '"';
        $rows = $this->fetchAll($where);

        if (!$rows) {
            //throw new Exception("Could not find row $contentId");
        }

        return $rows->toArray();
    }

    public function addPassword($id, $meetId, $contactId, $password)
    {
        // require_once('common.php'); //included in bootstrap
        $id = isset($id) ? $id : genId();

        $data = array(
            'id' => $id,
            'meetingInstanceId' => $meetId,
            'contactId' => $contactId,
            'meetingInstanceId_contactId' => $meetId.$contactId,
            'password' => $password
        );

        return $this->insert($data);
    }

    public function delPassword($id)
    {
        $this->delete('id ="' . $id . '" ');

    }

    public function delPasswordMeetingId($id)
    {
        $this->delete('meetingInstanceId ="' . $id . '" ');
    }
    
}
