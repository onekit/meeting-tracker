<?php

class Application_Model_DbTable_TopicInstance extends Zend_Db_Table_Abstract
{

    protected $_name = 'topicinstance';


			public function getTopic($id)
			{
                $row = $this->fetchRow('id = ' . $id);
                if (!$row) {
                throw new Exception("Could not find row $id");
                }

    			return $row->toArray();
			}

			public function getTopicMeetingId($id)
			{
                $where = 'meetingInstanceId ="' . $id .'"';
                $order = 'order ASC';
                $rows = $this->fetchAll($where, $order);

                if (!$rows) {
                throw new Exception("Could not find row $id");
                }

    			return $rows->toArray();
			}

    		public function addTopic($id, $title, $order, $meetId)
			{
	           $data = array(
                'id' => $id,
                'title' => $title,
                'order' => $order,
                'meetingInstanceId' => $meetId
                );
    			$this->insert($data);
    		}


			public function delTopic($id)
			{
			    $this->delete('id ="' . $id .'" ');
			}

            public function delTopicMeetingId($id)
			{
			    $this->delete('meetingInstanceId ="' . $id .'" ');
			}


}
