<?php

class Application_Model_DbTable_Comment extends Zend_Db_Table_Abstract
{

    protected $_name = 'comment';


    public function getComment($id)
    {

        $row = $this->fetchRow('id = "' . $id . '" ');
        if (!$row) {

            $result = null;

        } else {
            $result = $row->toArray();
        }
        return $result;
    }


    public function getCommentContentId($meetId, $contentId)
    {


        $select = $this->select()
                ->setIntegrityCheck(false)
                ->from(array('c' => 'comment'), array('c.id', 'c.submitTime', 'c.ownerId', 'c.content'))
                ->joinleft(array('u' => 'contact'), 'u.contactId = c.ownerId AND u.meetingInstanceId = "' . $meetId . '"', array('u.email', 'u.name'))
                ->where('c.contentId = ?', $contentId)
                ->where('c.delete = ?', 0)
                ->order(array('c.submitTime ASC'));
        $rows = $this->fetchAll($select);


        if (!$rows) {
            throw new Exception("Could not find row $contentId");
        }

        return $rows->toArray();
    }

    public function addComment($id, $contentId, $submitTime, $content, $ownerId)
    {
        // require_once('common.php'); //included in bootstrap
        $id = isset($id) ? $id : genId();

        $data = array(
            'id' => $id,
            'contentId' => $contentId,
            'submitTime' => $submitTime,
            'content' => $content,
            'ownerId' => $ownerId
        );

        return $this->insert($data);
    }

    public function delComment($id)
    {
        $this->delete('id ="' . $id . '" ');
    }

    public function delCommentSafe($id)
    {
        $data = array('delete' => 1);
        $this->update($data, 'id ="' . $id . '" ');

    }

    public function delCommentOwnerId($id)
    {
        $this->delete('ownerId ="' . $id . '" ');

    }

    public function restoreComment($id)
    {
        $data = array('delete' => 0);
        $this->update($data, 'id ="' . $id . '" ');

    }

    public function count($meetId)
    { //count all comments for selected meetId


        $select = $this->select()
                ->from(array('c' => 'comment'), array('c.contentId', 'COUNT(c.contentId) AS counter'))
                ->joinleft(array('r' => 'record'), 'c.contentId = r.id', array())
                ->joinleft(array('t' => 'topicinstance'), 'c.contentId = t.id', array())
                ->where('c.delete = ?', 0)
                ->where('r.meetingInstanceId = ?', $meetId)
                ->orWhere('t.meetingInstanceId = ?', $meetId)
                ->where('c.delete = ?', 0)
                ->group(array('c.contentId'))
                ->order(array('c.contentId ASC'))
                ->setIntegrityCheck(false);


        $rows = $this->fetchAll($select);

        if (!$rows) {
            //throw new Exception("Could not find row $meetId");
            $result = null;
        } else $result = $rows->toArray();

        return $result;
    }


}
