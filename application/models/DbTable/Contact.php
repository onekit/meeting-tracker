<?php

class Application_Model_DbTable_Contact extends Zend_Db_Table_Abstract
{

    protected $_name = 'contact';


    public function getContact($contactId)
    {

        $row = $this->fetchRow('contactId = "' . $contactId . '" ');
        if (!$row) {
            throw new Exception("Could not find row $contactId");
        }

        return $row->toArray();
    }

    public function getParticipantMeetingId($id)
    {

        $where = 'meetingInstanceId ="' . $id . '" AND isParticipant=1';
        $order = 'name ASC';
        $rows = $this->fetchAll($where, $order);

        if (!$rows) {
            throw new Exception("Could not find row $id");
        }

        return $rows->toArray();
    }

    public function getRecipientMeetingId($id)
    {

        $where = 'meetingInstanceId ="' . $id . '" AND isRecipient=1';
        $order = 'name ASC';
        $rows = $this->fetchAll($where, $order);

        if (!$rows) {
            throw new Exception("Could not find row $id");
        }

        return $rows->toArray();
    }


    public function addContact($contactId, $name, $email, $isParticipant, $isRecipient, $meetId)
    {
        $data = array(
            'id'=> genId(),
            'contactId' => $contactId,
            'name' => $name,
            'email' => $email,
            'isParticipant' => $isParticipant ? 1 : 0,
            'isRecipient' => $isRecipient ? 1 : 0,
            'meetingInstanceId' => $meetId
        );
        $this->insert($data);
    }


    public function delContact($id)
    {
        $this->delete('contactId ="' . $id . '" ');
    }

    public function delContactMeetingId($id)
    {
        $this->delete('meetingInstanceId ="' . $id . '" ');
    }


}
