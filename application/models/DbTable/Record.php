<?php

class Application_Model_DbTable_Record extends Zend_Db_Table_Abstract
{

    protected $_name = 'record';


			public function getRecord($id)
			{
                
                $row = $this->fetchRow('id = ' . $id);
                if (!$row) {
                throw new Exception("Could not find row $id");
                }

    			return $row->toArray();
			}


            public function getRecordsTopic($id)
			{

                $where = 'topicInstanceId ="' . $id .'"';
                $order = 'order ASC';
                $rows = $this->fetchAll($where, $order);

                if (!$rows) {
                throw new Exception("Could not find row $id");
                }

    			return $rows->toArray();

			}



    		public function addRecord($id, $type, $order, $provider, $content, $topicId, $meetId)
			{
                
               $type = strlen($type) ? $type : "text";
               $provider = strlen($provider) ? $provider : "default";
	           $data = array(
                'id' => $id,
                'type' => $type,
                'order' => $order,
                'content' => $content,
                'provider' => $provider,
                'topicInstanceId' => $topicId,
                'meetingInstanceId' => $meetId
                );
    			$this->insert($data);
    		}


            public function delRecord($id)
            {
                $this->delete('id ="' . $id .'" ');
            }

            public function delRecordMeetingId($id)
            {
                $this->delete('meetingInstanceId ="' . $id .'" ');
            }

		

}
