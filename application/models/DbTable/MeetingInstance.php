<?php

class Application_Model_DbTable_MeetingInstance extends Zend_Db_Table_Abstract
{

    protected $_name = 'meetinginstance';


    public function getMeetingInstance($id)
    {

        $row = $this->fetchRow('id = "' . $id . '" ');

        if (!$row) {
            //throw new Exception("Could not find MeetingInstance with id ".$id);
            $result = array();
        } else {
            $result = $row->toArray();
        }

        return $result;
    }


    public function addMeetingInstance($id, $title, $ownerId, $startTime, $expirationDate)
    {

        $data = array(
            'id' => $id,
            'title' => $title,
            'ownerId' => $ownerId,
            'startTime' => $startTime,
            'expirationDate' => $expirationDate
        );
        $this->insert($data);

    }

    public function updateMeetingInstance($id, $artist, $title, $creation_date)
    {
        $data = array(
            'artist' => $artist,
            'title' => $title,
            'creation_date' => $creation_date,
        );
        $this->update($data, 'id = ' . (int)$id);
    }

    public function delMeetingInstance($id)
    {

        $dbContacts = new Application_Model_DbTable_Contact();
        $dbTopics = new Application_Model_DbTable_TopicInstance();
        $dbRecords = new Application_Model_DbTable_Record();
        $dbPasswords = new Application_Model_DbTable_MeetingPassword();

        //$dbComments = new Application_Model_DbTable_Comment();
        /*
        $contacts = $dbContacts->getParticipantMeetingId($id);
        foreach ($contacts as $contact) { //clean all comments
            $dbComments->delCommentOwnerId($contact['contactId']);
        }
        */

        $dbTopics->delTopicMeetingId($id);
        $dbRecords->delRecordMeetingId($id);
        $dbPasswords->delPasswordMeetingId($id);
        $dbContacts->delContactMeetingId($id);

        $this->delete('id ="' . $id . '" ');

    }


}
