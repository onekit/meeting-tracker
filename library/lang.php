<?php

  function language_detect($text) {

            //English letters
            preg_match_all("/[a-zA-Z]/", $text, $result);
            $count['en'] = count($result[0]);

            //Cyrillic letters
            preg_match_all("/[�-��-߸�]/",$text, $result);
            $count['ru'] = count($result[0]);

            $max = max($count);
            $langCode = array_search($max,$count);

            return $langCode;
        }


?>