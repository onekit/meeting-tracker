<?php
function genId() 
    {
        //AlphaNumerical ID generating
        $u = sha1(uniqid());
        $id = substr($u,0,8).'-'.substr($u,8,4).'-'.substr($u,12,4).'-'.substr($u,16,4).'-'.substr($u,18,12);
        return $id;
    }

function genPass()
    {
        //new Password generating
        $u = sha1(uniqid());
        $pass = substr($u,0,8);
        return $pass;
    }

function hashPass($pass){ //hashing password to save it in db
    $pass = md5($pass);
    return $pass;
}

function cid($id)
    {
        //$result = str_replace("-","",$id);
        $result = trim($id);

        return $result;
    }


function defaultFormatDate($timestamp) {
    return date('d-m-Y h:i:s', $timestamp);
}
?>