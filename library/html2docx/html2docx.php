<?php
class html2docx
{
/* Here is collected 2 useful classes for conversion HTML to DOCX file.

Used libraries:
* /simplehtmldom
* /PHPWord
Author: aHarbunou


Here is not full list of tags, need to complete

*/


    function convert($url)
    {

        global $section, $tmp_array;


        $body = file_get_contents($url);

        /* //that patch was added, because PHPFog.com blocked fopen();
        $client = new Zend_Http_Client();
        $client->setUri($url);
        $response = $client->request('GET');
        $body = $response->getBody();
        */

        // Include the PHPWord.php, all other classes were loaded by an auto-loader
        require_once ('phpword/PHPWord.php');
        $PHPWord = new PHPWord();
        $PHPWord->addParagraphStyle('pStyle', array('spacing'=>100));
        $section = $PHPWord->createSection();


        require_once('simplehtmldom/simple_html_dom.php');
        $html = new simple_html_dom();
        $html->load($body);



        function html2docx_callback($element)
        {
            global $section, $tmp_array;

            $h1StyleFont = array('bold' => true, 'size' => 30, 'name' => 'Arial');
            $h1StyleParagraph = array('align' => 'center', 'spaceAfter' => 100);

            $h2StyleFont = array('bold' => true, 'size' => 26, 'name' => 'Arial');
            $h2StyleParagraph = array('align' => 'center', 'spaceAfter' => 100);

            $h3StyleFont = array('bold' => true, 'size' => 16, 'name' => 'Arial');
            $h3StyleParagraph = array('align' => 'left', 'spaceAfter' => 10);

            $pStyleFont = array('bold' => false, 'size' => 12, 'name' => 'Arial');
            $pStyleParagraph = array('align' => 'left');

            $spanStyleFont = array('bold' => false, 'size' => 12, 'name' => 'Arial');
            $spanStyleParagraph = array('align' => 'left');

            $iStyleFont = array('bold' => true, 'italic'=>true, 'size' => 12, 'name' => 'Arial');
            $iStyleParagraph = array('align' => 'left');

            $strongStyleFont = array('bold' => true, 'size' => 12, 'name' => 'Arial');
            $strongStyleParagraph = array('align' => 'left', 'spaceAfter' => 10);

            $liStyleFont = array('bold' => false, 'size' => 12, 'name' => 'Arial');
            $liStyleParagraph = array('listType'=>PHPWord_Style_ListItem::TYPE_NUMBER);


            if ($element->tag == 'h1') {
                $section->addTextBreak(1);
                $section->addText($element->plaintext, $h1StyleFont, $h1StyleParagraph);
            }

            if ($element->tag == 'h2') {
                $section->addTextBreak(1);
                $section->addText($element->plaintext, $h2StyleFont, $h2StyleParagraph);
            }

            if ($element->tag == 'h3') {
                $section->addTextBreak(1);
                $section->addText($element->plaintext, $h3StyleFont, $h3StyleParagraph);
            }

            if ($element->tag == 'p') {
                $section->addText($element->plaintext, $pStyleFont, $pStyleParagraph);
            }

            if ($element->tag == 'span') {
                $section->addText($element->plaintext, $spanStyleFont, $spanStyleParagraph);
            }

            if ($element->tag == 'a') {
                $section->addLink($element->href, "", $pStyleFont, $pStyleParagraph);
            }

            if ($element->tag == 'strong') {
                $section->addText($element->plaintext, $strongStyleFont, $strongStyleParagraph);
            }

            if ($element->tag == 'i') {
                $section->addText($element->plaintext, $iStyleFont, $iStyleParagraph);
            }

            if ($element->tag == 'img') {

                //PHPWord can't get images from remote URL, so that we download it to temp for awhile
                $tmpName = tempnam("tmp://", "img");
                $tmp_array[] = $tmpName;
                $imageContent = file_get_contents($element->src);
                $sourceFile = pathinfo($element->src);
                $tmpName = $tmpName . "." . $sourceFile['extension'];
                file_put_contents($tmpName, $imageContent);
                $section->addImage($tmpName);
                $tmp_array[] = $tmpName;

            }

            if ($element->tag == 'li') {
                $section->addListItem(htmlspecialchars_decode($element->plaintext), 0, $liStyleFont, $liStyleParagraph);
            }
            /*
            if ($element->tag == 'ul') {
                $section->addTextBreak(1);
            }
            */



        }
        
        $html->set_callback('html2docx_callback');
        $html->save();


        // At least write the document to webspace:
        $objWriter = PHPWord_IOFactory::createWriter($PHPWord, 'Word2007');
        ob_start();
        $objWriter->save('php://output');
        $result = ob_get_contents();
        ob_end_clean();


        //purge temp
        if (isset($tmp_array)) {
            foreach ($tmp_array as $tmp) {
                unlink($tmp);
            }
        }


        return $result;

    }

}

?>
	