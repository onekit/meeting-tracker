SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Table structure for table `comment`
--

CREATE TABLE IF NOT EXISTS `comment` (
  `id` varchar(36) NOT NULL,
  `contentId` varchar(36) default NULL,
  `submitTime` int(36) default NULL,
  `content` text,
  `ownerId` varchar(36) default NULL,
  `delete` tinyint(4) default '0',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `contact`
--

CREATE TABLE IF NOT EXISTS `contact` (
  `id` varchar(36) NOT NULL,
  `contactId` varchar(36) NOT NULL,
  `name` varchar(255) default NULL,
  `email` varchar(255) default NULL,
  `isParticipant` tinyint(1) default NULL,
  `isRecipient` tinyint(1) default NULL,
  `meetingInstanceId` varchar(36) default NULL COMMENT 'parent id',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `contact`
--

-- --------------------------------------------------------

--
-- Table structure for table `meetinginstance`
--

CREATE TABLE IF NOT EXISTS `meetinginstance` (
  `id` varchar(36) NOT NULL COMMENT 'id (alphanumberical)',
  `title` varchar(255) default NULL COMMENT 'meeting title',
  `ownerId` varchar(36) default NULL COMMENT 'member id',
  `startTime` int(16) default NULL COMMENT 'timestamp',
  `expirationDate` int(16) default NULL COMMENT 'timestamp',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `meetinginstance`
--


--
-- Table structure for table `meetingpassword`
--

CREATE TABLE IF NOT EXISTS `meetingpassword` (
  `id` varchar(36) NOT NULL COMMENT 'id (alphanumberical)',
  `meetingInstanceId` varchar(36) default NULL,
  `contactId` varchar(36) default NULL,
  `meetingInstanceId_contactId` varchar(72) NOT NULL,
  `password` varchar(36) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `meetingpassword`
--

-- --------------------------------------------------------

--
-- Table structure for table `record`
--

CREATE TABLE IF NOT EXISTS `record` (
  `id` varchar(36) NOT NULL,
  `type` varchar(36) default 'text',
  `order` int(16) default NULL,
  `provider` varchar(36) default NULL,
  `content` text,
  `topicInstanceId` varchar(36) default NULL,
  `meetingInstanceId` varchar(36) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `record`
--


--
-- Table structure for table `topicinstance`
--

CREATE TABLE IF NOT EXISTS `topicinstance` (
  `id` varchar(36) NOT NULL,
  `title` varchar(255) default NULL,
  `order` int(32) default NULL,
  `meetingInstanceId` varchar(36) default NULL COMMENT 'parent id',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `topicinstance`
--

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
